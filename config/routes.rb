Rails.application.routes.draw do

  devise_for :users,
    path: '', # optional namespace or empty string for no space
    path_names: {
      sign_in: 'login',
      sign_out: 'logout',
      password: 'secret',
      confirmation: 'verification',
      registration: 'register',
      sign_up: 'signup'
    }

  devise_scope :user do
    authenticated :user do
      root 'insureds#index', as: :authenticated_root
    end
  
    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

  

  resources :insureds, only: %i[index new create show] do
    resources :contacts, only: %i[index new create show]
    resources :addresses
    resources :orders
  end
  resources :orders, only: %i[index]
  resources :insured_records, only: [:new, :create]
  resources :pre_opinions
  resources :opinions
  resources :doctors
  resources :providers
  resources :treatments
  resources :raw_materials
  # resources :sections, only: %i[new create]

  resources :users, only: %i[index new create edit update]
  
  get 'pages/index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
