require 'test_helper'

class ContactInfoControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get contact_info_new_url
    assert_response :success
  end

  test "should get index" do
    get contact_info_index_url
    assert_response :success
  end

end
