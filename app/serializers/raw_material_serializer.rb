class RawMaterialSerializer < ActiveModel::Serializer
  attributes :id, :gnp_key, :sku, :short_description, :description, :raw_material_group

end
