class ItemSerializer < ActiveModel::Serializer
  attributes :id, :quantity, :raw_material

  def raw_material
    instance_options[:without_serializer] ? object.raw_material : RawMaterialSerializer.new(object.raw_material, without_serializer: true)
  end
end
