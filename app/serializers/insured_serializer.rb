class InsuredSerializer < ActiveModel::Serializer
  attributes :id, :name, :last_name, :mother_last_name
end
