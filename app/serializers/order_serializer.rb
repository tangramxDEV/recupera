class OrderSerializer < ActiveModel::Serializer
  attributes :id, :status, :created_at
  
  # has_many :raw_materials, each_serializer: RawMaterialSerializer
  has_many :items, each_serializer: ItemSerializer
  has_many :comments, each_serializer: CommentSerializer
  has_many :documents, each_serializer: DocumentSerializer
  belongs_to :insured
end
