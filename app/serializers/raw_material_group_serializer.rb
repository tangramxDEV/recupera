class RawMaterialGroupSerializer < ActiveModel::Serializer
  attributes :id

  has_one :raw_material, serializer: RawMaterialSerializer
end
