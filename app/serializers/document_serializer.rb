class DocumentSerializer < ActiveModel::Serializer
  attributes :id, :preview, :stored_at, :document_type
end
