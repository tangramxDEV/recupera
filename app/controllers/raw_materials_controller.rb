class RawMaterialsController < ApplicationController
    protect_from_forgery except: %i[index]
    def index
        @materials = RawMaterial.all
        
        respond_to do |format|
            format.js { render json: @materials, status: :ok }
            format.html
        end         
    end
end
