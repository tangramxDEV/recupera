class TreatmentsController < ApplicationController

    def create
        params[:doctors].each do |doctor|
            @treatment = Treatment.new(
                doctor_id: doctor, 
                reason: params[:reason],
                insured_id: params[:insured]
            )
            if @treatment.save
                status = true
                render json: { status: :ok }
            else
                return render json: { status: :unprocessable_entity }
            end
        end
    end

    private

    def treatment_params
        params.require(:treatment).permit!
    end
end
