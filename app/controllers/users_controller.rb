class UsersController < ApplicationController
    before_action :set_user, only: %i[edit update]

    def index
        @users = User.all
    end

    def new
        @user = User.new
    end

    def create
        @user = User.new user_params
        if params[:permissions]
            params[:permissions].map { |permission|
                @user.add_role permission.to_sym
            }
        end
        if @user.save
            return redirect_to users_url
        end
        render :new
    end

    def edit
    end

    def update
        @user.roles.map { |role| 
            @user.remove_role role.name.to_sym
        }
        if params[:permissions]
            params[:permissions].map { |permission|
                @user.add_role permission.to_sym
            }
        end
        if @user.update user_params
            return redirect_to users_url
        end
        render :edit
    end

    private
    def set_user
        @user = User.find params[:id]
    end
    def user_params
        params.require(:user).permit!
    end
end
