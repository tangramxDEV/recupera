class ContactsController < ApplicationController
  def index
    if params[:insured_id]  
     @contacts = Contact.where(insured_id: params[:insured_id])
    end
  end

  def new
    @insured = Insured.find params[:insured_id]
    @contact = @insured.contacts.new
    
  end

  def create
    @insured = Insured.find params[:insured_id]
    @contact = @insured.contacts.new(contact_params)

    if @contact.save
      return redirect_to insured_contacts_url
    else
      render :new
    end
  end

  private

  def contact_params
    params.require(:contact).permit(:name, :relation, :phone, :cell_phone, :email, :notes)
  end
end
