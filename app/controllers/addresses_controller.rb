class AddressesController < ApplicationController
  def index
    if params[:insured_id]  
      @addresses = Address.where(insured_id: params[:insured_id])
    end
  end

  def new
    @insured = Insured.find params[:insured_id]
    @address = @insured.addresses.new
  end

  def create
    @insured = Insured.find params[:insured_id]
    @address = @insured.addresses.new address_params

    if @address.save
      return redirect_to insured_addresses_url
    else
      render :new
    end
  end

  private

  def address_params
    params.require(:address).permit!
  end
end
