class InsuredsController < ApplicationController
    protect_from_forgery except: %i[index]

    def index
        @insureds = Insured.all()
        respond_to do |format|
            format.js { render json: @insureds, status: :ok }
            format.html
        end 
    end

    def new
        @insured = Insured.new 
    end

    def create
        #render plain: params[:insured].inspect
        @insured = Insured.new insured_params
        if @insured.save
            return redirect_to @insured
        end
        render :new
    end

    def show
        @insured = Insured.find params[:id]
    end

    private

    def insured_params
        params.require(:insured).permit! 
    end
end
