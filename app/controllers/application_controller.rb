class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if:  :devise_controller?
  layout :layout_by_resource


  protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:email, :password, :section_id])
  end

    private

    def layout_by_resource
        if devise_controller?
            'landing_page'
          else
            'application'
          end
    end
end
