class InsuredRecordsController < ApplicationController
    def new
        @insured_record = InsuredRecord.new
    end

    def create
        @insured_record = InsuredRecord.new insured_record_params
        if @insured_record.save
            return redirect_to insureds_path
          else
            render :new
          end
    end

    private

    def insured_record_params
        params.require(:insured_record).permit! 
    end
end
