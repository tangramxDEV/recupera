class OrdersController < ApplicationController
    # %w array of strings ['', '', '']
    # %i array of symbols
    before_action :set_insured, only: %i[index create show update]
    before_action :set_order, only: %i[show update]
    protect_from_forgery except: %i[show index]

    def index
        if @insured
            @orders = @insured.orders.includes(:items)
        else
            @orders = Order.all.includes(:insured).as_json(include: { insured: { only: %i[id name last_name mother_last_name] } })
        end
        respond_to do |format|
            # format.js { render json: @order, status: :ok }
            format.js { render json: @orders, status: :ok }
            format.html
        end 
    end

    def new
        @sections = Section.all
        @materials = RawMaterial.includes(:raw_material_group, :unit_measurement, :raw_material_family)
    end

    def show
        respond_to do |format|
            format.js { render json: @order, status: :ok }
            format.html
        end 
        # render json: @order, status: :ok
    end

    def create
        @order = @insured.orders.new order_params
        @comment = @order.comments.new comments_params
        if params[:document]
            @document = @order.documents.new documents_params
        end
        params[:item].map do |item|
            @item = @order.items.new(
                raw_material_id: item[:raw_material_id],
                quantity: item[:quantity]
            )
        end
        
        if params[:document]
            if @order.save and @comment.save and @item.save and @document.save
                return render json: { status: :ok }
            end
        else
            if @order.save and @comment.save and @item.save
                return render json: { status: :ok }
            end
        end
        render json: { status: :unprocessable_entity }

    end

    def update
        @comment = @order.comments.new comments_params
        if params[:document]
            @document = @order.documents.create! documents_params
        end
        params[:item].map do |item|
            @item = Item.find item[:item_id]
            if item[:destroy]
                @item.destroy
            else
                @item.update(
                    quantity: item[:quantity]
                )
            end
        end
        if @comment.save and @order.update order_params
            return render json: { status: :ok }
        end
        render json: { status: :unprocessable_entity }
    end

    private

    def items_params
        params.permit(:item).permit!
    end

    def comments_params
        params.require(:comment).permit!
    end

    def documents_params
        params.require(:document).permit!
    end

    def order_params
        params.require(:order).permit!
    end

    def set_insured
        if params[:insured_id]
            @insured = Insured.find params[:insured_id]
        end
    end

    def set_order
        @order = Order.find params[:id]
    end

end
