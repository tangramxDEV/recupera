class ProvidersController < ApplicationController
  def index
    @providers = Provider.all
  end

  def new
    @provider = Provider.new
  end

  def create
    @provider = Provider.new provider_params
    if @provider.save
        return redirect_to providers_path
    end
    render :new
  end

  private
  def provider_params
    params.require(:provider).permit! 
  end
end
