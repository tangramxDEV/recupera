class ContactInfoController < ApplicationController
  def new
  end

  def show
    @contacts = ContactInfo.where(insured_id: params[:id])
    render json: { contacts: @contacts }
  end

  def index
    @contacts = ContactInfo.where(insured_id: params[:insured_id])
    render json: { contacts: @contacts }
  end

  def create
    @contact = ContactInfo.new contact_params
      if @contact.save
        return render json: { status: 201 }
      end
      render json: { status: 400 }
  end

  private

  def contact_params
    params.require(:contact_info).permit!
  end
end
