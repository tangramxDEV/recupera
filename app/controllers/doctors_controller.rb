class DoctorsController < ApplicationController

    def index
        @doctors = Doctor.all

        respond_to do |format|
            format.json { render :json => { doctors: @doctors } }
            format.html
        end 
        
    end

    def new
        @doctor = Doctor.new
    end

    def create
        @doctor = Doctor.new doctor_params
        if @doctor.save
            return redirect_to doctors_path
        end
        render :new
    end

    private

    def doctor_params
        params.require(:doctor).permit! 
    end
end
