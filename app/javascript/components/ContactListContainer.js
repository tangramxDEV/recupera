import React, { useEffect, useState } from 'react'
import { URL } from '../settings/configs'

const ContactListContainer = ({insured}) => {
    const [ contacts, setContact ] = useState([])

    useEffect(() => {
        const fetchResource = async () => {
            try {
                let res = await fetch(`${URL}/contact_info/${insured}`)
                let data = await res.json()
                setContact(data.contacts)
            } catch (error) {
                console.log(error)
            }
        }
        fetchResource()
    }, [])

    return <>
        <div className="row">
            { contacts.map((contact) => (
                <div className="col-sm-6" key={contact.id}>
                    <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">{contact.street}</h5>
                        <p className="card-text"># {contact.interior_number}</p>
                        <p className="card-text"># Exterior {contact.outdoor_number}</p>
                    </div>
                    </div>
                </div>
            ))}
        </div>
    </>
}

export default ContactListContainer