import React, { useState, useEffect } from 'react'
import { URL } from '../settings/configs'
import useSearcher from '../hooks/useSearcher'

const NewOrder = () => {
    const [insureds, setInsureds] = useState([])
    const { query, setQuery, filteredInsureds } = useSearcher(insureds)

    useEffect(() => {
        const fetchResource = async () => {
            try {
                let res = await fetch(`${URL}/insureds`)
                let data = await res.json()
                console.log(data)
                setInsureds(data)
            } catch (error) {
                console.log(error)
            }
        }
        fetchResource()
    }, [])

    if(filteredInsureds.length == 0){
        return <>
        <a className="float-right btn btn-primary" data-toggle="collapse" href="#collapseOrder" role="button" aria-expanded="false" aria-controls="collapseOrder">
        NUEVA SOLICITUD
    </a>
    <br/><br/><br/>
    <div className="collapse" id="collapseOrder">
    <div className="card">
        <div className="card-header">
            <div className="row">
                <div className="col sm-3">
                    Asegurados
                </div>
                <div className="col sm-9">
                    <input 
                        type="text" 
                        placeholder="Buscar"
                        className="form-control"
                        value={query}
                        onChange={(e) => {
                        setQuery(e.target.value)
                        }}
                    />
                </div>
            </div>
        </div>
        </div>
            <p>
            No se encontro ningun asegurado 
            <a href="/insureds/new"> Crearlo</a>
            </p>
        </div>
        </>
    }

    return <>
        <a className="float-right btn btn-primary" data-toggle="collapse" href="#collapseOrder" role="button" aria-expanded="false" aria-controls="collapseOrder">
            NUEVA SOLICITUD
        </a>
        <br/><br/><br/>
        <div className="collapse" id="collapseOrder">
        <div className="card">
            <div className="card-header">
                <div className="row">
                    <div className="col sm-3">
                        Asegurados
                    </div>
                    <div className="col sm-9">
                        <input 
                            type="text" 
                            placeholder="Buscar"
                            className="form-control"
                            value={query}
                            onChange={(e) => {
                            setQuery(e.target.value)
                            }}
                        />
                    </div>
                </div>
            </div>
            <ul className="list-group list-group-flush">
                { filteredInsureds.map((insured) => (
                    <li className="list-group-item">
                        {insured.name} {insured.last_name} {insured.mother_last_name}

                        <a href={`/insureds/${insured.id}/orders/new`} className="btn btn-primary float-right" data-toggle="tooltip" title="Solicitud">
                            <i className="fab fa-telegram-plane"></i>
                        </a>
                    </li>
                )) }
            </ul>
        </div>
        </div>
    </>
}

export default NewOrder