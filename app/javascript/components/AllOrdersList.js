import React from 'react'
import useOrderSearcher from '../hooks/useOrderSearcher'

const AllOrdersList = ({orders}) => {
    const { query, setQuery, filteredOrders } = useOrderSearcher(orders)
    return <React.Fragment>
        <div className="form-group">
            <input 
                type="text" 
                className="form-control"
                placeholder="BUSCAR SOLICITUD"
                value={query}
                onChange={(e) => {
                    setQuery(e.target.value)
                }}
            />
        </div>
        <table className="table table-bordered">
            <thead>
                <tr>
                <th scope="col">Ticket</th>
                <th scope="col">A. PATERNO</th>
                <th scope="col">A. MATERNO</th>
                <th scope="col">NOMBRE</th>
                <th scope="col">FECHA SOLICITUD</th>
                <th scope="col">USUARIO</th>
                <th scope="col">MODULO</th>
                <th scope="col">TIPO DE SERVICIO</th>
                <th scope="col">ACCIONES</th>
                </tr>
            </thead>
            <tbody>
            { filteredOrders.map((order) => (
                <tr key={order.id}>
                    <th>CPSA99671</th>
                    <td>{ order.insured.name }</td>
                    <td>{ order.insured.last_name }</td>
                    <td>{ order.insured.mother_last_name }</td>
                    <td>{ order.created_at }</td>
                    <td>INAJERA</td>
                    <td>M3C</td>
                    <td>INSUMOS</td>
                    <td>
                        <a href={`/insureds/${order.insured.id}/orders/${order.id}`}>Ir</a>
                    </td>
                </tr>
            )) }
            </tbody>
        </table>
    </React.Fragment>
}

export default AllOrdersList