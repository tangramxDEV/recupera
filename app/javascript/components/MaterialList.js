import React, { useState, useEffect } from 'react'
import useMaterialSearcher from '../hooks/useMaterialSearcher'
import Dictator from '../uicomponents/dictator'
import OrderHistory from '../uicomponents/order-history'
import Binnacle from '../uicomponents/binnacle'
import OrderTable from '../uicomponents/order-table'
import InputSearcher from '../uicomponents/input-searcher'
import FilteredMaterials from '../uicomponents/filtered-materiales'
import Dropbox from 'dropbox'
import { URL, DROP_BOX_ACCESS_TOKEN } from '../settings/configs'

const MaterialList = ({sections, section, authenticity_token, insured}) => {
    const [files, setFiles] = useState([])
    const [selectDocument, setSelectDocument] = useState(null)
    const [uploadBtn, setUploadBtn] = useState('visible')
    const [loader, setLoader] = useState('hidden')
    const [materials, setMaterials] = useState([]) // Insumos Disponibles
    const [orderMaterials, setOrderMaterials] = useState([]) // Insumos de solicitud actual
    const [oldOrders, setOldOrders] = useState([]) // Insumos de solicitudes anteriores
    const [comment, setComment] = useState('')
    const { query, setQuery, filteredMaterials } = useMaterialSearcher(materials) // Filter para Insumos Disponibles

    
    useEffect(() => {
        const fetchMaterials = async () => {
            try {
                let res = await fetch(`${URL}/raw_materials`)
                let data = await res.json()
                console.log(data)
                setMaterials(data)
            } catch (error) {
                console.log(error)
            }
        }
        const fetchResource = async () => {
            try {
                let res = await fetch(`${URL}/insureds/${insured}/orders`)
                let data = await res.json()
                setOldOrders(data)
            } catch (error) {
                console.log(error)
            }
        }
        fetchMaterials()
        fetchResource()
    }, [])

    const handleIncrement = (e) => {
        setOrderMaterials(orderMaterials.map((material) => {
            if(material.sku === e.target.name)
                material.quantity = e.target.value
            
            return material
        }))
    }

    const handleTrash = (material) => {
        setOrderMaterials(orderMaterials.filter((currentMeterial) => (
            currentMeterial !== material
        )))
    }

    const handleComment= e => {
        setComment(e.target.value)
    }

    const handleAddMaterialCollection = (materials) => {
        let olderMaterials = []
        materials.map((material) => {
            
            if(!orderMaterials.some(current_material => current_material.sku === material.raw_material.sku)){
                material.raw_material.quantity = material.quantity
                olderMaterials.push(material.raw_material)
            }
        })
        setOrderMaterials([
            ...orderMaterials,
            ...olderMaterials
        ])
        
    }

    const handleClick = (material) => {
        material.quantity = 1
        if(!orderMaterials.some(current_material => current_material.sku === material.sku)){
            setOrderMaterials([
                ...orderMaterials,
                material
            ])
        }
    }
    const onChangeFileInput = e => {
        // console.log(uploadBtn)
        // if(uploadBtn === 'hidden'){
        //     setUploadBtn('visible')
        // } else {
        //     setUploadBtn('hidden')
        // }
    }
    const handleUploadFile = e => {
        setLoader('visible')

        let date = Date.now();
        let dbx = new Dropbox.Dropbox({accessToken: DROP_BOX_ACCESS_TOKEN})
        let fileInput = document.getElementById('fileInput')
        let file = fileInput.files[0]

        dbx.filesUpload({path: `/documents/${date}${file.name}`, contents: file})
        .then((res) => {
            setLoader('hidden')
            setFiles([{
                stored_at: res.client_modified,
                preview: res.name,
                document_type: selectDocument
            }])
            console.log(res);
          })
          .catch((error) => {
            setLoader('hidden')
            console.error(error);
          });
    }
    const handleSelectDocumentChange = (e) => {
        setSelectDocument(e.target.value)
    }
    const handleSubmit = async () => {
        
        let item = []

        orderMaterials.map((material) => {
            item.push({
                raw_material_id: material.id,
                quantity: material.quantity
            })
        })
        
        let data = {
            order: {
                status: 'pre-dictamen'
            },
            comment: {
                comment
            },
            item,
            authenticity_token
        }
        if(files[0]){
            data['document'] = {
                document_type: files[0].document_type ? files[0].document_type : '',
                preview: files[0].preview ? files[0].preview : '',
                stored_at: files[0].stored_at ? files[0].stored_at : ''
            }
        }

        console.log(data)

        try{
            let config = {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
            
            await fetch(`${URL}/insureds/${insured}/orders`, config).then((res) => {
                if(res.status == '200')
                    window.location.href = `/insureds/${insured}`
            })

        } catch (error) {
           console.log(error)
        }
    }

    return <>
        
        <Dictator
            section={section}
            sections={sections}
        />
        
        <OrderHistory
            oldOrders={oldOrders}
            handleAddMaterials={handleAddMaterialCollection}
        />

        <InputSearcher   
            placeholder={'BUSCAR INSUMOS Y/O SERVICIOS'}
            query={query}
            setQuery={setQuery}
        />
        <FilteredMaterials
            filteredMaterials={filteredMaterials}
            handleClick={handleClick}
        />
        <br/><br/>
        <OrderTable 
            orderMaterials={orderMaterials}
            handleIncrement={handleIncrement}
            handleTrash={handleTrash}
        />
        <br/><br/>
        <h2>Documentos</h2>
        <div className="row">
            <div className="col sm-4">
                <div className="form-group">
                    <label htmlFor="exampleFormControlSelect1">Tipo de documento</label>
                    <select 
                        className="form-control" 
                        id="exampleFormControlSelect1"
                        name={selectDocument}
                        onChange={handleSelectDocumentChange}
                    >
                        <option disabled selected value> -- selecciona una opción -- </option>
                        <option value="CARTA DE AUTORIZACION">CARTA DE AUTORIZACION</option>
                        <option value="CORREO INICIAL">CORREO INICIAL</option>
                        <option value="REPROCESO">REPROCESO</option>
                        <option value="RECETA">RECETA</option>
                        <option value="CUSE DE ENTREGA">ACUSE DE ENTREGA</option>
                        <option value="OTROS">OTROS</option>
                    </select>
                </div>
            </div>
            <div className="col sm-4">
                <div className="form-group">
                    <input 
                        type="file" 
                        className="space-t form-control-file" 
                        id="fileInput" 
                        onChange={onChangeFileInput}
                    />
                </div>
            </div>
            <div className="col sm-4 space-t-x">
                <button 
                    className="btn btn-outline-primary"
                    onClick={handleUploadFile}
                    style={{visibility: uploadBtn}}
                >
                    Subir
                </button>

                <div 
                    className="space-l spinner-border text-primary" 
                    role="status"
                    style={{visibility: loader}}
                >
                    <span className="sr-only">Loading...</span>
                </div>
            </div>

            <table className="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">DOCUMENTO</th>
                    <th scope="col">USUARIO</th>
                    <th scope="col">FECHA</th>
                    <th scope="col">OPCIONES</th>
                </tr>
            </thead>
            <tbody>
            { files.map((file) => (
                <tr>
                    <td>{ selectDocument }</td>
                    <td>INAGERA</td>
                    <td>{ file.stored_at }</td>
                    <td>
                    <a
                    onClick={(e) => {
                        e.preventDefault()
                        window.open(`https://www.dropbox.com/home/Aplicaciones/Recupera/documents?preview=${file.preview}`)
                    }}
                        href="#"
                    >
                        <i className="fas fa-eye"></i>
                        
                    </a>
                    </td>
                </tr>
            ))}
            </tbody>
        </table>
        </div>
       

        <Binnacle 
            comment={comment}
            handleComment={handleComment}
        />
            <button 
                type="button" 
                className="btn btn-primary float-right"
                onClick={handleSubmit}
            >
                ENVIAR A PRE-DICTAMEN
            </button>
            <br/>
    </>
}

export default MaterialList