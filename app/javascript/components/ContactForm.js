import React from 'react'

const ContactForm = ({ onChange, onSubmit, form}) => (
    <form onSubmit={onSubmit}>
        <div className="form-group">
            <label>CALLE</label>
            <input 
                className="form-control" 
                name="street"
                onChange={onChange}
                value={form.street}
            />
        </div>
        <div className="form-group">
            <label>NUMERO INTERIOR</label>
            <input 
                className="form-control"
                id="interior_number" 
                name="interior_number"
                onChange={onChange}
                value={form.interior_number}
            />
        </div>
        <div className="form-group">
            <label>NUMERO EXTERIOR</label>
            <input 
                className="form-control" 
                id="outdoor_number"
                name="outdoor_number"
                onChange={onChange}
                value={form.outdoor_number} 
            />
        </div>
        <div className="form-group">
            <label>CODIGO POSTAL</label>
            <input 
                className="form-control" 
                id="postal_code" 
                name="postal_code"
                onChange={onChange}
                value={form.postal_code} 
            />
        </div>
        <div className="form-group">
            <label>COLONIA</label>
            <input 
                className="form-control" 
                id="suburb"
                name="suburb"
                onChange={onChange}
                value={form.suburb} 
            />
        </div>
        <button type="submit" className="btn btn-primary">Submit</button>
    </form>
)

export default ContactForm