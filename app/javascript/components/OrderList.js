import React, { useState, useEffect } from 'react'
import Dropbox from 'dropbox'
import { URL, DROP_BOX_ACCESS_TOKEN } from '../settings/configs'

const OrderList = ({orderId, insuredId, authenticity_token}) => {
    const [selectDocument, setSelectDocument] = useState(null)
    const [uploadBtn, setUploadBtn] = useState('visible')
    const [loader, setLoader] = useState('hidden')
    const [insured, setInsured] = useState({})
    const [items, setItems] = useState([])
    const [itemsDeleted, setItemsDeleted] = useState([])
    const [comments, setComments] = useState([])
    const [newFiles, setNewFiles] = useState([])
    const [files, setFiles] = useState([])
    const [comment, setComment] = useState('')

    const handleSelectDocumentChange = (e) => {
        setSelectDocument(e.target.value)
    }
    const onChangeFileInput = e => {
        
    }
    const handleUploadFile = e => {
        setLoader('visible')

        let date = Date.now();
        let dbx = new Dropbox.Dropbox({accessToken: DROP_BOX_ACCESS_TOKEN})
        let fileInput = document.getElementById('fileInput')
        let file = fileInput.files[0]

        dbx.filesUpload({path: `/documents/${date}${file.name}`, contents: file})
        .then((res) => {
            let file = {
                stored_at: res.client_modified,
                preview: res.name,
                document_type: selectDocument
            }
            setLoader('hidden')
            setFiles([
                ...files,
                file
            ])
            setNewFiles([file])
            console.log(res);
          })
          .catch((error) => {
            setLoader('hidden')
            console.error(error);
          });
    }

    const handleComment= e => {
        setComment(e.target.value)
    }

    const handleSubmit = async () => {
        let item = []
        items.map((current_item) => {
            if(!current_item.raw_material.quantity)
                current_item.raw_material.quantity = current_item.quantity
            
            item.push({
                item_id: current_item.id,
                raw_material_id: current_item.raw_material.id,
                quantity: current_item.raw_material.quantity
            })
        })
        
        itemsDeleted.map((deleted) => {
            item.push({
                item_id: deleted.id,
                destroy: true
            })
        })

        

        let data = {
            order: {
                status: 'dictamen'
            },
            comment: {
                comment
            },
            item,
            authenticity_token
        }

        if(newFiles[0]){
            data['document'] = {
                document_type: newFiles[0].document_type ? newFiles[0].document_type : '',
                preview: newFiles[0].preview ? newFiles[0].preview : '',
                stored_at: newFiles[0].stored_at ? newFiles[0].stored_at : ''
            }
        }
        
        try{
            let config = {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
            // /insureds/:insured_id/orders/:id
            await fetch(`${URL}/insureds/${insuredId}/orders/${orderId}`, config).then((res) => {
                // console.log(res)
                if(res.status == '200')
                    window.location.href = `/insureds/${insuredId}`
            })

        } catch (error) {
           console.log(error)
        }

        console.log(data)
        console.log('submiting...')
    }

    const handleIncrement = (e) => {
        setItems(items.map((item) => {
            if(item.raw_material.sku === e.target.name)
                item.raw_material.quantity = e.target.value
            
            return item
        }))
    }

    const handleTrash = (item) => {
        setItemsDeleted([
            ...itemsDeleted,
            item
        ])
        setItems(items.filter((currentItem) => (
            currentItem.raw_material.sku !== item.raw_material.sku
        )))
    }

    useEffect(() => {
        const fetchResource = async () => {
            try {
                let res = await fetch(`${URL}/insureds/${insuredId}/orders/${orderId}`)
                let data = await res.json()
                console.log(data)
                setFiles(data.documents)
                setInsured(data.insured)
                setItems(data.items)
                setComments(data.comments)
            } catch (error) {
                console.log(error)
            }
        }
        fetchResource()
    }, [])

    return <div>
        <h4>PRE-DICTAMEN DEL ASEGURADO: <strong>{insured.name} {insured.last_name} { insured.mother_last_name }</strong></h4>

        <br/><br/>
        <h4>Pedido: </h4>
        <div>
            <table className="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">SKU</th>
                        <th scope="col">TIPO</th>
                        <th scope="col">DESCRIPCION</th>
                        <th scope="col">CANTIDAD</th>
                        <th scope="col">ACCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    { items.map((item) => (
                        <tr>
                            <th>{item.raw_material.sku}</th>
                            <td>{item.raw_material.raw_material_group.description}</td>
                            <td>{item.raw_material.short_description}</td>
                            <td>
                                <input 
                                    className="form-control" 
                                    type="number" 
                                    min="1" 
                                    defaultValue={item.quantity}
                                    name={item.raw_material.sku}
                                    onChange={handleIncrement}
                                />
                            </td>
                            <td>
                                <i 
                                    className="fas fa-trash"
                                    onClick={()=>{handleTrash(item)}}
                                ></i>
                            </td>
                        </tr>
                    )) }
                </tbody>
            </table>
        </div>

        <br/><br/>
        <h2>Documentos</h2>
        <div className="row">
            <div className="col sm-4">
                <div className="form-group">
                    <label htmlFor="exampleFormControlSelect1">Tipo de documento</label>
                    <select 
                        className="form-control" 
                        id="exampleFormControlSelect1"
                        name={selectDocument}
                        onChange={handleSelectDocumentChange}
                    >
                        <option disabled selected value> -- selecciona una opción -- </option>
                        <option value="CARTA DE AUTORIZACION">CARTA DE AUTORIZACION</option>
                        <option value="CORREO INICIAL">CORREO INICIAL</option>
                        <option value="REPROCESO">REPROCESO</option>
                        <option value="RECETA">RECETA</option>
                        <option value="CUSE DE ENTREGA">ACUSE DE ENTREGA</option>
                        <option value="OTROS">OTROS</option>
                    </select>
                </div>
            </div>
            <div className="col sm-4">
                <div className="form-group">
                    <input 
                        type="file" 
                        className="space-t form-control-file" 
                        id="fileInput" 
                        onChange={onChangeFileInput}
                    />
                </div>
            </div>
            <div className="col sm-4 space-t-x">
                <button 
                    className="btn btn-outline-primary"
                    onClick={handleUploadFile}
                    style={{visibility: uploadBtn}}
                >
                    Subir
                </button>

                <div 
                    className="space-l spinner-border text-primary" 
                    role="status"
                    style={{visibility: loader}}
                >
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        </div>
        <table className="table table-bordered">
        <thead>
            <tr>
                <th scope="col">DOCUMENTO</th>
                <th scope="col">USUARIO</th>
                <th scope="col">FECHA</th>
                <th scope="col">OPCIONES</th>
            </tr>
        </thead>
        <tbody>
        { files.map((file) => (
            <tr>
                <td>{ file.document_type }</td>
                <td>INAGERA</td>
                <td>{ file.stored_at }</td>
                <td>
                <a
                    onClick={(e) => {
                        e.preventDefault()
                        window.open(`https://www.dropbox.com/home/Aplicaciones/Recupera/documents?preview=${file.preview}`)
                    }}
                    href="#"
                >
                    <i className="fas fa-eye"></i>
                    
                </a>
                </td>
            </tr>
        ))}
        </tbody>
    </table>

        <br/><br/>
        <h2>Bitacora de eventos registrados</h2>
        <br/>
        <input 
            type="text" 
            className="form-control" 
            name="comment"
            value={comment}
            onChange={handleComment}
        />
        <br/>
        <table className="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">USUARIO</th>
                        <th scope="col">MODULO</th>
                        <th scope="col">FECHA</th>
                        <th scope="col">COMENTARIOS</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>PreDictamentUser</td>
                        <td>SEGUIMIENTO</td>
                        <td>27/05/19</td>
                        <td>{comment}</td>
                    </tr>
                    { comments.map((current_comment) => (
                        <tr>
                        <td>INAJERA</td>
                        <td>SEGUIMIENTO</td>
                        <td>{current_comment.created_at}</td>
                        <td>{current_comment.comment}</td>
                    </tr>
                    )) }
                </tbody>
            </table>
            <br/>
            <button 
                type="button" 
                className="btn btn-primary float-right"
                onClick={handleSubmit}
            >
                ENVIAR A DICTAMEN
            </button>
            <br/>

    </div>
}

export default OrderList