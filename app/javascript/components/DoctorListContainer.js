import React, { useEffect, useState } from 'react'
import { URL } from '../settings/configs'

const DoctorListContainer = ({authenticity_token, insured}) => {
    const [doctors, setDoctor] = useState([])
    const [form, setForm] =  useState({reason: ''})
    const [checkeds, setCheck] = useState([])
    

    useEffect(() => {
        const fetchResource = async () => {
            try {
                let res = await fetch(`${URL}/doctors`)
                let data = await res.json()
                
                setDoctor(data.doctors)
            } catch (error) {
                console.log(error)
            }
        }
        fetchResource()
    }, [])

    const handleSubmit = async e => {
        e.preventDefault()
        let data = {
            doctors: checkeds,
            reason: form.reason,
            authenticity_token,
            insured
        }
        
        try{
            let config = {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${authenticity_token}`
                },
                body: JSON.stringify(data)
            }

            await fetch(`${URL}/treatments`, config)
        } catch (error) {
           console.log(error)
        }
    }

    const handleChange = e => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }
    const handleDoctorChecked = e => {
        const options = checkeds
        let index
        // check if the check box is checked or unchecked
        if (e.target.checked) {
          // add the numerical value of the checkbox to options array
          options.push(+e.target.value)
        } else {
          // or remove the value from the unchecked checkbox from the array
          index = options.indexOf(+e.target.value)
          options.splice(index, 1)
        }
        setCheck(options)
    }

    return <>
        <ul className="list-group">
            { doctors.map((doctor) => (
                <li 
                    key={doctor.id}
                    className="list-group-item"
                >
                    { doctor.name } {doctor.last_name}
                    <div className="form-check">
                        <input 
                            className="form-check-input position-static" 
                            type="checkbox" 
                            name={doctor.id}
                            value={doctor.id} 
                            onChange={handleDoctorChecked}
                        />
                    </div>
                </li>
            ))}
        </ul>
        <form onSubmit={handleSubmit}>
            <div className="form-group">
                <label htmlFor="exampleInputEmail1">Motivo</label>
                <input 
                    type="text" 
                    className="form-control" 
                    onChange={handleChange}
                    name="reason"
                    value={form.reason}
                />
            </div>
            <button type="submit" className="btn btn-primary">Asignar</button>
        </form>
    </>

}


export default DoctorListContainer