import React, { useState } from 'react'
import ContactForm from './ContactForm'
import { URL } from '../settings/configs'

const ContactContainer = ({authenticity_token, insured}) => {
    const [form, setForm] =  useState({street: '',
                                interior_number: '',
                                outdoor_number: '',
                                postal_code: '',
                                suburb: '',  
                                authenticity_token: authenticity_token,
                                insured_id: insured})

    const handleChange = e => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }
    //contact_info/new
    const handleSubmit = async e => {
        e.preventDefault()
        console.log(form)
        try{
            let config = {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${authenticity_token}`
                },
                body: JSON.stringify(form)
            }

            await fetch(`${URL}/contact_info`, config)
        } catch (error) {
           console.log(error)
        }
    }

    return <ContactForm 
        form={form}
        onChange={handleChange}
        onSubmit={handleSubmit}
        token={authenticity_token} 
    />
}


export default ContactContainer