import React from "react"
import useSearcher from '../hooks/useSearcher'

const InsuredList = ({insureds}) => {
  const { query, setQuery, filteredInsureds } = useSearcher(insureds)
  
  if(filteredInsureds.length == 0){
    return <>
      <div className="form-group">
        <input 
          type="text" 
          placeholder="Buscar"
          className="form-control"
          value={query}
          onChange={(e) => {
            setQuery(e.target.value)
          }}
        />
      </div>
      <p>
        No se encontro ningun asegurado 
        <a href="/insureds/new"> Crearlo</a>
      </p>
    </>
  }
  return <>
    <br/>
    
    <div className="form-group">
      <input 
        type="text" 
        placeholder="Buscar"
        className="form-control"
        value={query}
        onChange={(e) => {
          setQuery(e.target.value)
        }}
      />
    </div>
      <table className="table">
        <thead>
          <tr>
              <th>Nombre</th>
              <th>Apellido Paterno</th>
              <th>Apellido Materno</th>
              <th>Género</th>
              <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          { filteredInsureds.map(insured =>(
            <tr key={insured.id}>
              <td>{insured.name}</td>
              <td>{insured.last_name}</td>
              <td>{insured.mother_last_name}</td>
              <td>{insured.gender}</td>
              <td>
                <a href={`/insureds/${insured.id}`} className="btn btn-primary space-r" data-toggle="tooltip" title="Ver">
                  <i className="fas fa-eye"></i>
                </a>
                
                <a href={`/insureds/${insured.id}/orders/new`} className="btn btn-primary" data-toggle="tooltip" title="Solicitud">
                  <i className="fab fa-telegram-plane"></i>
                </a>
              </td>
            </tr>
          )) }
        </tbody>
    </table>
  </>
}
    
export default InsuredList
