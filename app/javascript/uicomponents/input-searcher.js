import React from 'react'

const InputSearcher = ({placeholder, query, setQuery}) => (
    <div className="form-group">
        <input 
            type="text" 
            className="form-control"
            placeholder={placeholder}
            value={query}
            onChange={(e) => {
                setQuery(e.target.value)
            }}
        />
    </div>
)

export default InputSearcher