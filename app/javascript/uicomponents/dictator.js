import React from 'react'

const Dictator = ({section, sections}) => (
    <div className="row">
        <div className="col sm-4">
            <div className="form-group">
                <label>Modulo</label>
                <p>{section}</p>
            </div>
        </div>
        <div className="col sm-4">
            <div className="form-group">
                <label>Tipo de Solicitud</label>
                <select className="form-control">
                    <option value="1">INICIAL</option>
                    <option value="2">SUBSECUENTE</option>
                </select>
            </div>
        </div>
        <div className="col sm-4">
            <div className="form-group">
                <label>Tipo de servicio</label>
                <select className="form-control">
                    <option value="1">INSUMOS</option>
                </select>
            </div>
        </div>
    </div>
)

export default Dictator