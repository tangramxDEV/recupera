import React from 'react'

const FilteredMaterials = ({filteredMaterials, handleClick}) => (
    <div style={{'maxHeight':'350px'}} className="pre-scrollable accordion" id="accordionExample">
        { filteredMaterials.map((material) => (
            <div className="card" key={material.id}>
                <div className="card-header" id="headingOne">
                    <h2 className="mb-0">
                        <button className="btn btn-link" type="button" data-toggle="collapse" data-target={`#${material.sku}`} aria-expanded="true" aria-controls={`${material.sku}`}>
                            {material.short_description}
                        </button>
                        <button 
                            type="button" 
                            className="btn btn-outline-primary float-right"
                            onClick={()=>{handleClick(material)}}
                        >
                            <i className="fas fa-plus"></i>
                        </button>
                    </h2>
                </div>

                <div id={`${material.sku}`} className="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div className="card-body">
                        {material.description}
                    </div>
                </div>
            </div>
        )) }
    </div>
)

export default FilteredMaterials