import React from 'react'

const OrderTable = ({orderMaterials, handleIncrement, handleTrash}) => (
<>
    <h4>Pedido: </h4>
        <div>
            <table className="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">SKU</th>
                        <th scope="col">TIPO</th>
                        <th scope="col">DESCRIPCION</th>
                        <th scope="col">CANTIDAD</th>
                        <th scope="col">ACCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    { orderMaterials.map((material) => (
                        <tr key={material.id}>
                            <th>{material.sku}</th>
                            <td>{material.raw_material_group.description}</td>
                            <td>{material.short_description}</td>
                            <td>
                                <input 
                                    className="form-control" 
                                    type="number" 
                                    min="1" 
                                    defaultValue={material.quantity || '1'}
                                    name={material.sku}
                                    onChange={handleIncrement}
                                />
                            </td>
                            <td>
                                <i 
                                    className="fas fa-trash"
                                    onClick={()=>{handleTrash(material)}}
                                ></i>
                            </td>
                        </tr>
                    )) }
                </tbody>
            </table>
        </div>
</>
)

export default OrderTable