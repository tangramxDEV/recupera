import React from 'react'

const Binnacle = ({comment, handleComment}) => (
<>
    <h2>Bitacora de eventos registrados</h2>
    <br/>
    <input 
        type="text" 
        className="form-control" 
        name="comment"
        value={comment}
        onChange={handleComment}
    />
    <br/>
    <table className="table table-bordered">
        <thead>
            <tr>
                <th scope="col">USUARIO</th>
                <th scope="col">MODULO</th>
                <th scope="col">FECHA</th>
                <th scope="col">COMENTARIOS</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>INAJERA</td>
                <td>SEGUIMIENTO</td>
                <td>27/05/19</td>
                <td>{comment}</td>
            </tr>
        </tbody>
    </table>
    <br/>
</>
)

export default Binnacle