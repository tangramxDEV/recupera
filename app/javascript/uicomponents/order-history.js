import React from 'react'

const OrderHistory = ({oldOrders, handleAddMaterials}) => (
<>
    <a className="btn btn-outline-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
        HISTORICO DE INSUMOS
    </a>
    <br/>
    <div className="collapse" id="collapseExample">
    <br/>
    <div className="card card-body">
        <div className="accordion" id="accordionExample">
        { oldOrders.map((order) => (
            <div className="card">
                <div className="card-header" id="headingOne">
                    <h2 className="mb-0">
                        <button className="btn btn-link" type="button" data-toggle="collapse" data-target={`#collapse${order.id}`} aria-expanded="true" aria-controls={`collapse${order.id}`}>
                            Solicitud {order.created_at}
                        </button>
                        <button 
                            type="button" 
                            className="btn btn-outline-primary float-right"
                            onClick={()=>{
                                handleAddMaterials(order.items)
                            }}
                        >
                            <i className="fas fa-plus"></i>
                        </button>
                    </h2>
                </div>

                <div id={`collapse${order.id}`} className="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div className="card-body">
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">SKU</th>
                                    <th scope="col">TIPO</th>
                                    <th scope="col">DESCRIPCION</th>
                                    <th scope="col">CANTIDAD</th>
                                </tr>
                            </thead>
                            <tbody>
                                {order.items.map((item) => (
                                    <tr>
                                        <th>{item.raw_material.sku}</th>
                                        <td>{item.raw_material.raw_material_group.description}</td>
                                        <td>{item.raw_material.short_description}</td>
                                        <td>{item.quantity}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )) }
        </div>
    </div>
    </div>
    <br/>
</>
)

export default OrderHistory