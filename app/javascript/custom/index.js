$(document).on('turbolinks:load', function() {
  $('.tag-tooltip').tooltip();
    $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
        $("body").toggleClass("sidebar-toggled");
        $(".sidebar").toggleClass("toggled");
        if ($(".sidebar").hasClass("toggled")) {
          $('#nav_logo').attr('src','logo_white_collapse.png');
          $("#nav_logo").removeClass("nav_logo");
          $("#nav_logo").addClass("nav_logo_collapse");
          
          $('.sidebar .collapse').collapse('hide');
        } else {
          $('#nav_logo').attr('src','logo_white.png');
          $("#nav_logo").removeClass("nav_logo_collapse");
          $("#nav_logo").addClass("nav_logo");
        }
      });


    })
