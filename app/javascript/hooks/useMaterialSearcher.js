import { useState, useMemo } from 'react'

const useMaterialSearcher = (materials) => {
    const [query, setQuery] = useState('')
    const [filteredMaterials, setFilteredMaterials] = useState([materials])
  
    useMemo(() => {
      const result = materials.filter(material => {
      return `${material.short_description}`
        .toLowerCase()
        .includes(query.toLowerCase())
    })
    setFilteredMaterials(result)
    }, [materials, query])

    return { query, setQuery, filteredMaterials }
}

export default useMaterialSearcher