import { useState, useMemo } from 'react'

const useSearcher = (insureds) => {
    const [query, setQuery] = useState('')
    const [filteredInsureds, setFilteredInsureds] = useState([insureds])
  
    useMemo(() => {
      const result = insureds.filter(insureds => {
      return `${insureds.name} ${insureds.last_name} ${insureds.mother_last_name}`
        .toLowerCase()
        .includes(query.toLowerCase())
    })
    setFilteredInsureds(result)
    }, [insureds, query])

    return { query, setQuery, filteredInsureds }
}

export default useSearcher