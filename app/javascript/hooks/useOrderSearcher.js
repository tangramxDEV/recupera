import { useState, useMemo } from 'react'

const useOrderSearcher = (orders) => {
    const [query, setQuery] = useState('')
    const [filteredOrders, setFilteredOrders] = useState(orders)
  
    useMemo(() => {
      const result = orders.filter(order => {
      return `${order.insured.name} ${order.insured.last_name}`
        .toLowerCase()
        .includes(query.toLowerCase())
    })
    setFilteredOrders(result)
    }, [orders, query])

    return { query, setQuery, filteredOrders }
}

export default useOrderSearcher