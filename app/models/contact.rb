class Contact < ApplicationRecord
  belongs_to :insured

  validates :name, presence: true
  validates :relation, presence: true
  validates :phone, presence: true
  validates :cell_phone, presence: true
  validates :email, presence: true
  validates :notes, presence: true

  accepts_nested_attributes_for :insured

end
