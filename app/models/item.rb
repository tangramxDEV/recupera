class Item < ApplicationRecord
  belongs_to :order
  belongs_to :raw_material
end
