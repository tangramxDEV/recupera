class Doctor < ApplicationRecord
    has_many :treatments
    has_many :insureds, through: :treatments
    
    validates :name, 
        :last_name, 
        :mother_last_name, 
        :mobile,
        :mail,
        presence: true 
    
end
