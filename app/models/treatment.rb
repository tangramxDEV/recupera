class Treatment < ApplicationRecord
  belongs_to :insured
  belongs_to :doctor
end
