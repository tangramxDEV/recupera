class RawMaterial < ApplicationRecord
  belongs_to :unit_measurement
  belongs_to :raw_material_group
  belongs_to :raw_material_family

  has_many :items
  has_many :orders, through: :items
end
