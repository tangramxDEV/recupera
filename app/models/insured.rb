class Insured < ApplicationRecord
    has_one :contact_info
    has_many :treatments
    has_many :doctors, through: :treatments
    
    has_many :contacts
    has_many :addresses
    has_many :orders
    
    validates :name, presence: true 
    validates :last_name, presence: true 
    validates :mother_last_name, presence: true 
    validates :gender, presence: true 
    validates :birth_date, presence: true 
    validates :policy, presence: true 
end
