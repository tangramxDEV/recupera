class Order < ApplicationRecord
  belongs_to :insured
  has_many :comments
  has_many :documents
  
  has_many :items
  has_many :raw_materials, through: :items
  accepts_nested_attributes_for :raw_materials
end
