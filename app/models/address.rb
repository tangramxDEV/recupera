class Address < ApplicationRecord
  belongs_to :insured

  validates :street, presence: true
  validates :interior_number, presence: true
  validates :outdoor_number, presence: true
  validates :postal_code, presence: true
  validates :suburb, presence: true
  validates :reference, presence: true

end
