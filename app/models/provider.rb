class Provider < ApplicationRecord
    validates :business_name, presence: true 
    validates :tradename, presence: true
    validates :rfc, presence: true
    validates :address, presence: true
    validates :street, presence: true
    validates :suburb, presence: true
    validates :responsable, presence: true
    validates :responsible_phone, presence: true
    validates :email, presence: true
    validates :notification_mail, presence: true
    validates :general_description, presence: true
    
end
