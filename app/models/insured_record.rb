class InsuredRecord
    include ActiveModel::Model

    attr_accessor(
        :name, 
        :last_name, 
        :mother_last_name, 
        :gender, 
        :birth_date, 
        :street, 
        :interior_number, 
        :outdoor_number, 
        :postal_code, 
        :suburb
    )
    validates :name, presence: true 
    validates :last_name, presence: true 
    validates :mother_last_name, presence: true 
    validates :gender, presence: true 
    validates :birth_date, presence: true 
    validates :street, presence: true 
    validates :interior_number, presence: true 
    validates :outdoor_number, presence: true 
    validates :postal_code, presence: true 
    validates :suburb, presence: true 

    
    def save
        return false if invalid?

        ActiveRecord::Base.transaction do
            insured = Insured.create!(name: name, 
                last_name: last_name, mother_last_name: mother_last_name, 
                gender: gender, birth_date: birth_date)
            insured.create_contact_info!(
                street: street, 
                interior_number: interior_number, 
                outdoor_number: outdoor_number, 
                postal_code: postal_code, 
                suburb: suburb
            )
        end

        true

    rescue ActiveRecord::StatementInvalid => e
        # Handle exception that caused the transaction to fail
        # e.message and e.cause.message can be helpful
        errors.add(:base, e.message)
        false
    end
end
