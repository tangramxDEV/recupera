class RawMaterialPrice < ApplicationRecord
  belongs_to :raw_material
  belongs_to :provider
end
