# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_07_054203) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string "street"
    t.string "interior_number"
    t.string "outdoor_number"
    t.string "postal_code"
    t.string "suburb"
    t.string "reference"
    t.bigint "insured_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["insured_id"], name: "index_addresses_on_insured_id"
  end

  create_table "comments", force: :cascade do |t|
    t.bigint "order_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "comment"
    t.index ["order_id"], name: "index_comments_on_order_id"
  end

  create_table "contact_infos", force: :cascade do |t|
    t.bigint "insured_id", null: false
    t.string "street"
    t.string "interior_number"
    t.string "outdoor_number"
    t.string "postal_code"
    t.string "suburb"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["insured_id"], name: "index_contact_infos_on_insured_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "name"
    t.string "relation"
    t.string "phone"
    t.string "cell_phone"
    t.string "email"
    t.string "notes"
    t.bigint "insured_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["insured_id"], name: "index_contacts_on_insured_id"
  end

  create_table "doctors", force: :cascade do |t|
    t.string "name"
    t.string "last_name"
    t.string "mother_last_name"
    t.string "mobile"
    t.string "mail"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "documents", force: :cascade do |t|
    t.bigint "order_id", null: false
    t.string "preview"
    t.date "stored_at"
    t.string "document_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["order_id"], name: "index_documents_on_order_id"
  end

  create_table "insureds", force: :cascade do |t|
    t.string "name"
    t.string "last_name"
    t.string "mother_last_name"
    t.string "gender"
    t.date "birth_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "policy"
  end

  create_table "items", force: :cascade do |t|
    t.bigint "order_id", null: false
    t.bigint "raw_material_id", null: false
    t.string "quantity"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["order_id"], name: "index_items_on_order_id"
    t.index ["raw_material_id"], name: "index_items_on_raw_material_id"
  end

  create_table "orders", force: :cascade do |t|
    t.string "status"
    t.bigint "insured_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["insured_id"], name: "index_orders_on_insured_id"
  end

  create_table "providers", force: :cascade do |t|
    t.string "business_name"
    t.string "tradename"
    t.string "rfc"
    t.string "address"
    t.string "street"
    t.string "suburb"
    t.string "responsable"
    t.string "responsible_phone"
    t.string "email"
    t.string "notification_mail"
    t.string "general_description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "raw_material_families", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "raw_material_groups", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "raw_material_prices", force: :cascade do |t|
    t.bigint "raw_material_id", null: false
    t.bigint "provider_id", null: false
    t.string "public_price"
    t.string "gnp_price"
    t.boolean "active"
    t.string "rfp"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["provider_id"], name: "index_raw_material_prices_on_provider_id"
    t.index ["raw_material_id"], name: "index_raw_material_prices_on_raw_material_id"
  end

  create_table "raw_materials", force: :cascade do |t|
    t.string "sku"
    t.string "short_description"
    t.string "description"
    t.string "variant"
    t.string "brand"
    t.bigint "unit_measurement_id", null: false
    t.bigint "raw_material_group_id", null: false
    t.bigint "raw_material_family_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "gnp_key"
    t.index ["raw_material_family_id"], name: "index_raw_materials_on_raw_material_family_id"
    t.index ["raw_material_group_id"], name: "index_raw_materials_on_raw_material_group_id"
    t.index ["unit_measurement_id"], name: "index_raw_materials_on_unit_measurement_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "sections", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "treatments", force: :cascade do |t|
    t.bigint "insured_id", null: false
    t.bigint "doctor_id", null: false
    t.string "reason"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["doctor_id"], name: "index_treatments_on_doctor_id"
    t.index ["insured_id"], name: "index_treatments_on_insured_id"
  end

  create_table "unit_measurements", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "section_id", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["section_id"], name: "index_users_on_section_id"
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  add_foreign_key "addresses", "insureds"
  add_foreign_key "comments", "orders"
  add_foreign_key "contact_infos", "insureds"
  add_foreign_key "contacts", "insureds"
  add_foreign_key "documents", "orders"
  add_foreign_key "items", "orders"
  add_foreign_key "items", "raw_materials"
  add_foreign_key "orders", "insureds"
  add_foreign_key "raw_material_prices", "providers"
  add_foreign_key "raw_material_prices", "raw_materials"
  add_foreign_key "raw_materials", "raw_material_families"
  add_foreign_key "raw_materials", "raw_material_groups"
  add_foreign_key "raw_materials", "unit_measurements"
  add_foreign_key "treatments", "doctors"
  add_foreign_key "treatments", "insureds"
  add_foreign_key "users", "sections"
end
