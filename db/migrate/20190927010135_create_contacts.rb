class CreateContacts < ActiveRecord::Migration[6.0]
  def change
    create_table :contacts do |t|
      t.string :name
      t.string :relation
      t.string :phone
      t.string :cell_phone
      t.string :email
      t.string :notes
      t.references :insured, null: false, foreign_key: true

      t.timestamps
    end
  end
end
