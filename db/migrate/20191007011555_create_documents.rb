class CreateDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :documents do |t|
      t.references :order, null: false, foreign_key: true
      t.string :preview
      t.date :stored_at
      t.string :document_type

      t.timestamps
    end
  end
end
