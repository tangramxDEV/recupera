class AddGnpKeyToRawMaterials < ActiveRecord::Migration[6.0]
  def change
    add_column :raw_materials, :gnp_key, :string
  end
end
