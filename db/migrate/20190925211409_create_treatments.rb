class CreateTreatments < ActiveRecord::Migration[6.0]
  def change
    create_table :treatments do |t|
      t.references :insured, null: false, foreign_key: true
      t.references :doctor, null: false, foreign_key: true
      t.string :reason

      t.timestamps
    end
  end
end
