class CreateProviders < ActiveRecord::Migration[6.0]
  def change
    create_table :providers do |t|
      t.string :business_name
      t.string :tradename
      t.string :rfc
      t.string :address
      t.string :street
      t.string :suburb
      t.string :responsable
      t.string :responsible_phone
      t.string :email
      t.string :notification_mail
      t.string :general_description

      t.timestamps
    end
  end
end
