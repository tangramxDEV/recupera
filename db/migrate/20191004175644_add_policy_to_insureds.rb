class AddPolicyToInsureds < ActiveRecord::Migration[6.0]
  def change
    add_column :insureds, :policy, :string
  end
end
