class CreateContactInfos < ActiveRecord::Migration[6.0]
  def change
    create_table :contact_infos do |t|
      t.references :insured, null: false, foreign_key: true
      t.string :street
      t.string :interior_number
      t.string :outdoor_number
      t.string :postal_code
      t.string :suburb

      t.timestamps
    end
  end
end
