class CreateRawMaterialFamilies < ActiveRecord::Migration[6.0]
  def change
    create_table :raw_material_families do |t|
      t.string :description

      t.timestamps
    end
  end
end
