class CreateItems < ActiveRecord::Migration[6.0]
  def change
    create_table :items do |t|
      t.references :order, null: false, foreign_key: true
      t.references :raw_material, null: false, foreign_key: true
      t.string :quantity

      t.timestamps
    end
  end
end
