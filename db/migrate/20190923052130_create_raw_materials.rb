class CreateRawMaterials < ActiveRecord::Migration[6.0]
  def change
    create_table :raw_materials do |t|
      t.string :sku
      t.string :short_description
      t.string :description
      t.string :variant
      t.string :brand
      t.references :unit_measurement, null: false, foreign_key: true
      t.references :raw_material_group, null: false, foreign_key: true
      t.references :raw_material_family, null: false, foreign_key: true

      t.timestamps
    end
  end
end
