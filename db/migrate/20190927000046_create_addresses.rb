class CreateAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :addresses do |t|
      t.string :street
      t.string :interior_number
      t.string :outdoor_number
      t.string :postal_code
      t.string :suburb
      t.string :reference
      t.references :insured, null: false, foreign_key: true

      t.timestamps
    end
  end
end
