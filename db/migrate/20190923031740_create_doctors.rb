class CreateDoctors < ActiveRecord::Migration[6.0]
  def change
    create_table :doctors do |t|
      t.string :name
      t.string :last_name
      t.string :mother_last_name
      t.string :mobile
      t.string :mail

      t.timestamps
    end
  end
end
