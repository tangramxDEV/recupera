class CreateInsureds < ActiveRecord::Migration[6.0]
  def change
    create_table :insureds do |t|
      t.string :name
      t.string :last_name
      t.string :mother_last_name
      t.string :gender
      t.date :birth_date

      t.timestamps
    end
  end
end
