class CreateRawMaterialGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :raw_material_groups do |t|
      t.string :description

      t.timestamps
    end
  end
end
