class CreateRawMaterialPrices < ActiveRecord::Migration[6.0]
  def change
    create_table :raw_material_prices do |t|
      t.references :raw_material, null: false, foreign_key: true
      t.references :provider, null: false, foreign_key: true
      t.string :public_price
      t.string :gnp_price
      t.boolean :active
      t.string :rfp

      t.timestamps
    end
  end
end
